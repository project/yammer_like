<?php
/**
 * @file
 * The module's main API functions and hook implementations.
 */

/**
 * Implements hook_menu().
 */
function yammer_like_menu() {
  $items['admin/config/services/yammer-like'] = array(
    'title' => 'Yammer Like',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('yammer_like_settings_form'),
    'access arguments' => array('administer yammer like'),
    'file' => 'yammer_like.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function yammer_like_permission() {
  return array(
    'administer yammer like' =>  array(
      'title' => t('Administer Yammer Like'),
      'description' => t('Perform administration tasks for "Yammer Like".'),
    ),
    'access yammer like button' =>  array(
      'title' => t('Access Yammer Like button'),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function yammer_like_block_info() {
  return array(
    'like' => array(
      'info' => t('Yammer Like button'),
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function yammer_like_block_view($delta = '') {
  $block = array();

  if (user_access('access yammer like button') && variable_get('yammer_like_network')) {
    $block['subject'] = '';
    $block['content'] = array(
      '#markup' => '<div id="yammer-like-block"></div>',
      '#attached' => array(
        'js' => array(
          'https://assets.yammer.com/assets/platform_embed.js' => array(
            'type' => 'external',
            'scope' => 'footer',
          ),
          'yam.connect.actionButton({
             container: "#yammer-like-block",
             network: "' . variable_get('yammer_like_network') . '",
             action: "like"
            });' => array(
            'type' => 'inline',
            'scope' => 'footer',
          ),
        ),
      ),
    );
  }

  return $block;
}

/**
 * Implements hook_page_alter().
 */
function yammer_like_page_alter(&$page) {
  if (variable_get('yammer_like_embed_content') && user_access('access yammer like button') && variable_get('yammer_like_network')) {
    $page['content']['yammer_like'] = array(
      '#markup' => '<div id="yammer-like-content"></div>',
      '#attached' => array(
        'js' => array(
          'https://assets.yammer.com/assets/platform_embed.js' => array(
            'type' => 'external',
            'scope' => 'footer',
          ),
          'yam.connect.actionButton({
             container: "#yammer-like-content",
             network: "' . variable_get('yammer_like_network') . '",
             action: "like"
            });' => array(
            'type' => 'inline',
            'scope' => 'footer',
          ),
        ),
      ),
    );
  }
}
