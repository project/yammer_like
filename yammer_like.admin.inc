<?php
/**
 * @file
 * The module's admin menu callbacks and administrative functions.
 */

/**
 * Generates yammer system settings form.
 */
function yammer_like_settings_form($form, &$form_state) {
  $form['yammer_like_network'] = array(
    '#type' => 'textfield',
    '#title' => t('Network'),
    '#description' => t('To retrieve the network permalink, please navigate to the feed in the Yammer web application and copy it from the URL. For example: example.com'),
    '#default_value' => variable_get('yammer_like_network'),
    '#required' => TRUE,
  );

  $form['yammer_like_embed_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add the link to the content region'),
    '#description' => t('If you do not want to use the block, you can embed the button to the content region.'),
    '#default_value' => variable_get('yammer_like_embed_content'),
  );

  return system_settings_form($form);
}
